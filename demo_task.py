def solve_quadratic(a, b, c):
    discriminant = b**2 - 4*a*c
    if discriminant < 0:
        return None
    elif discriminant == 0:
        x = -b / (2*a)
        return (x,)
    else:
        x1 = (-b + discriminant**0.5) / (2*a)
        x2 = (-b - discriminant**0.5) / (2*a)
        return tuple(sorted((x1, x2)))

def solution(a, b, c):
    solutions = solve_quadratic(a, b, c)
    if solutions:
        return solutions
    else:
        print("None")

# Examples
print(solution(a=1, b=6, c=5))   # Output: (-5, -1)
print(solution(a=1, b=4, c=4))   # Output: (-2,)
print(solution(a=1, b=6, c=45))  # Output: None
